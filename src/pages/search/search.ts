import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  searchQuery: any;
  items: any;
  sampledata;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.initializeItems();
  }

  initializeItems() {
    this.sampledata = [
      {
        item_name: 'Item aaa',
        item_thumbnail: 'https://images.pexels.com/photos/1142984/pexels-photo-1142984.jpeg',
      },
      {
        item_name: 'Item bbb',
        item_thumbnail: 'https://images.pexels.com/photos/610294/pexels-photo-610294.jpeg',
      },
      {
        item_name: 'Item ccc',
        item_thumbnail: 'https://images.pexels.com/photos/1327218/pexels-photo-1327218.jpeg',
      },
      {
        item_name: 'Item aaa2',
        item_thumbnail: 'https://images.pexels.com/photos/1142984/pexels-photo-1142984.jpeg',
      },
      {
        item_name: 'Item bbb2',
        item_thumbnail: 'https://images.pexels.com/photos/610294/pexels-photo-610294.jpeg',
      },
      {
        item_name: 'Item ccc2',
        item_thumbnail: 'https://images.pexels.com/photos/1327218/pexels-photo-1327218.jpeg',
      },
      {
        item_name: 'Item aaa3',
        item_thumbnail: 'https://images.pexels.com/photos/1142984/pexels-photo-1142984.jpeg',
      },
      {
        item_name: 'Item bbb3',
        item_thumbnail: 'https://images.pexels.com/photos/610294/pexels-photo-610294.jpeg',
      },
      {
        item_name: 'Item ccc3',
        item_thumbnail: 'https://images.pexels.com/photos/1327218/pexels-photo-1327218.jpeg',
      },
    ]
    console.log(this.items);
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.sampledata.filter((item) => {
        return (item.item_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

}
